//
//  main.cpp
//  ALCS
//
//  Created by Максим Гришкин on 27/04/15.
//  Copyright (c) 2015 Максим Гришкин. All rights reserved.
//

#include <iostream>
#include <string>
#include <gtest/gtest.h>
#include <stdlib.h>

#include "SlowALCS.h"
#include "FastALCS.h"

std::string randomString(size_t size) {
    std::string result = "";
    for (; size > 0; --size)
        result += (char)('a' + rand() % 26);
    return result;
}

bool equal(std::vector<std::vector<unsigned int>> first, std::vector<std::vector<unsigned int>> second) {
    if (first.size() != second.size())
        return false;
    size_t verticalIndex, horizontalIndex;
    for (verticalIndex = 0; verticalIndex < first.size(); ++verticalIndex) {
        if (first[verticalIndex].size() != second[verticalIndex].size())
            return false;
        for (horizontalIndex = 0; horizontalIndex < first[verticalIndex].size(); ++horizontalIndex) {
            if (first[verticalIndex][horizontalIndex] != second[verticalIndex][horizontalIndex])
                return false;
        }
    }
    return true;
}

class FastALCS_TestRandom: public testing::TestWithParam<size_t> {
};

TEST_P(FastALCS_TestRandom, TestRandom) {
    std::vector<std::vector<unsigned int>> FastALCS_Result, SlowALCS_Result;
    size_t testNumber = GetParam();
    const size_t maxSize = 100;
    std::string mainString, substringString;
    for (size_t test = 0; test < testNumber; ++test) {
        mainString = randomString(rand() % maxSize);
        substringString = randomString(rand() % maxSize);
        FastALCS_Result = NALCS::FastALCS(mainString, substringString);
        SlowALCS_Result = NALCS::SlowALCS(mainString, substringString);
        EXPECT_TRUE(equal(FastALCS_Result, SlowALCS_Result));
    }
    SUCCEED();
}

INSTANTIATE_TEST_CASE_P(TestRandomInst, FastALCS_TestRandom, testing::Values(1e1, 1e2, 1e3));

int main(int argc, char ** argv) {
    srand(time(NULL));
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
