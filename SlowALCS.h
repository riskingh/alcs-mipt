//
//  SlowALCS.h
//  ALCS
//
//  Created by Максим Гришкин on 27/04/15.
//  Copyright (c) 2015 Максим Гришкин. All rights reserved.
//

#ifndef __ALCS__SlowALCS__
#define __ALCS__SlowALCS__

#include <stdio.h>
#include <vector>
#include <string>
#include <algorithm>
#include <numeric>

namespace NALCS {
    void LCS(const std::string &mainString, const std::string &substringString, size_t substringBegin, std::vector<unsigned int>::iterator output) {
        size_t substringSize =  substringString.size() - substringBegin;
        std::vector<std::vector<unsigned int>> LCS(mainString.size() + 1, std::vector<unsigned int>(substringSize + 1, 0));
        size_t verticalIndex, horizontalIndex;
        for (verticalIndex = 1; verticalIndex <= mainString.size(); ++verticalIndex) {
            for (horizontalIndex = 1; horizontalIndex <= substringSize; ++horizontalIndex) {
                LCS[verticalIndex][horizontalIndex] = std::max(LCS[verticalIndex - 1][horizontalIndex], LCS[verticalIndex][horizontalIndex - 1]);
                LCS[verticalIndex][horizontalIndex] = std::max(LCS[verticalIndex][horizontalIndex], LCS[verticalIndex - 1][horizontalIndex - 1] + (int)(mainString[verticalIndex - 1] == substringString[horizontalIndex + substringBegin - 1]));
            }
        }
        for (horizontalIndex = 1; horizontalIndex <= substringSize; ++horizontalIndex)
            *output++ = LCS[mainString.size()][horizontalIndex];
    }
    
    std::vector<std::vector<unsigned int>> SlowALCS(const std::string &mainString, const std::string &substringString) {
        std::vector<std::vector<unsigned int>> ALCS(substringString.size() + 1, std::vector<unsigned int>(substringString.size() + 1, 0));
        size_t prefix;
        for (prefix = 0; prefix < substringString.size(); ++prefix)
            LCS(mainString, substringString, prefix, ALCS[prefix].begin() + prefix + 1);
        
        return ALCS;
    }
}

#endif /* defined(__ALCS__SlowALCS__) */
