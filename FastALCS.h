//
//  FastALCS.h
//  ALCS
//
//  Created by Максим Гришкин on 27/04/15.
//  Copyright (c) 2015 Максим Гришкин. All rights reserved.
//

#ifndef __ALCS__FastALCS__
#define __ALCS__FastALCS__

#include <stdio.h>
#include <vector>
#include <string>
#include <algorithm>
#include <numeric>

namespace NALCS {
    std::vector<std::vector<unsigned int>> FastALCS(const std::string &mainString, const std::string &substringString) {
        std::vector<std::vector<unsigned int>> vertical;
        vertical.assign(mainString.size() + 1, std::vector<unsigned int>(substringString.size() + 1, 0));
        std::vector<std::vector<unsigned int>> horizontal(vertical.begin(), vertical.end());
        
        std::iota(horizontal[0].begin(), horizontal[0].end(), 1);
        
        size_t verticalIndex, horizontalIndex;
        unsigned int verticalValue, horizontalValue;
        bool edge;
        for (verticalIndex = 0; verticalIndex < mainString.size(); ++verticalIndex) {
            for (horizontalIndex = 0; horizontalIndex < substringString.size(); ++horizontalIndex) {
                verticalValue = vertical[verticalIndex][horizontalIndex];
                horizontalValue = horizontal[verticalIndex][horizontalIndex];
                if (verticalValue == horizontalValue) {
                    vertical[verticalIndex][horizontalIndex + 1] = verticalValue;
                    horizontal[verticalIndex + 1][horizontalIndex] = horizontalValue;
                }
                else if (verticalValue < horizontalValue) {
                    edge = (mainString[verticalIndex] == substringString[horizontalIndex]);
                    vertical[verticalIndex][horizontalIndex + 1] = edge ? horizontalValue : verticalValue;
                    horizontal[verticalIndex + 1][horizontalIndex] = edge ? verticalValue : horizontalValue;
                }
                else if (verticalValue > horizontalValue) {
                    vertical[verticalIndex][horizontalIndex + 1] = horizontalValue;
                    horizontal[verticalIndex + 1][horizontalIndex] = verticalValue;
                }
            }
        }
        
        std::vector<std::vector<bool>> criticalPoint(substringString.size() + 1, std::vector<bool>(substringString.size() + 1, false));
        for (horizontalIndex = 0; horizontalIndex < substringString.size(); ++horizontalIndex) {
            if (horizontal[mainString.size()][horizontalIndex])
                criticalPoint[horizontal[mainString.size()][horizontalIndex]][horizontalIndex] = true;
        }
        
        std::vector<std::vector<unsigned int>> ALCS(substringString.size() + 1, std::vector<unsigned int>(substringString.size() + 1, 0));
        bool wasCriticalPoint;
        for (verticalIndex = substringString.size(); verticalIndex > 0; --verticalIndex) {
            wasCriticalPoint = criticalPoint[verticalIndex][verticalIndex - 1];
            for (horizontalIndex = verticalIndex; horizontalIndex <= substringString.size(); ++horizontalIndex) {
                ALCS[verticalIndex - 1][horizontalIndex] = wasCriticalPoint ? ALCS[verticalIndex][horizontalIndex] : ALCS[verticalIndex][horizontalIndex] + 1;
                wasCriticalPoint |= criticalPoint[verticalIndex][horizontalIndex];
            }
        }
        return ALCS;
    }
}

#endif /* defined(__ALCS__FastALCS__) */
